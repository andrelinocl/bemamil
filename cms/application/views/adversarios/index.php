<main>
    <div class="row">
        <div class="col-lg-12 bg--branco">
            <div class="page-header">
                <div class="row">
                    <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                        <h1>Adversários</h1>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                        <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target="#addAdversario">Adicionar Adversário</a>
                    </div>
                </div>
                <?php echo create_breadcrumb();?>
            </div>
            <div class="page-content">
                <?php echo isset($_SESSION['item']) ? $_SESSION['item'] : ''; ?>
                <table class="table table-responsive" id="lista-noticias">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Posição</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $var = '';?>
                        <?php foreach($lista_adversario as $linha): ?>
                        <tr>
                            <td><?php echo $linha->id_adversario;?></td>
                            <td>
                                <?php echo $linha->nome_adversario;?>
                            </td>
                            <td>
                                <a href="#" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#addPosicao"><i class="fa fa-pencil"></i> Editar</a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
<div id="addAdversario" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Adicionar um Adversário</h4>
            </div>
            <div class="modal-body">
                <?php
                echo form_open_multipart('adversarios/store', array('class'=>'form'));
                /* NOME DO TIME */
                echo form_label('Nome do time')."\r\n";
                echo form_input(array('name'=>'nome_adversario', 'id'=>'nome_adversario', 'class'=>'form-control'),set_value('nome_adversario', isset($busca) ? $busca->nome_adversario : ''));
                echo form_error('nome_adversario', '<div class="alert alert-warning" role="alert">','</div>');
                /* NOME DO TIME */

                /* IMAGEM DO TIME */
                echo form_label('Logo');
                echo form_upload(array('name'=>'imagem', 'id'=>'imgInp', 'class'=>'form-control'), set_value('imagem', isset($busca) ? $busca->imagem : ''));
                /* IMAGEM DO TIME */

                echo form_hidden('id', isset($busca) ? $busca->id : '');
                echo form_button(array(
                    'class' => 'btn btn-success',
                    'content' => '<span><i class="fa fa-floppy-o" aria-hidden="true"></i></span><span>Salvar</span>',
                    'type' => 'submit',
                ));
                echo form_close();?>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
