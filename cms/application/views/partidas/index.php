<main>
    <div class="row">
        <div class="col-lg-12 bg--branco">
            <div class="page-header">
                <div class="row">
                    <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                        <h1>Partidas</h1>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                        <?php echo anchor('partidas/adicionar', 'nova partida', array('class'=>'btn btn-primary btn-block'));?>
                    </div>
                </div>
                <?php echo create_breadcrumb();?>
            </div>
            <div class="page-content">
                <?php echo isset($_SESSION['item']) ? $_SESSION['item'] : ''; ?>
                <table class="table table-responsive" id="lista-noticias">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Adversário</th>
                            <th>Tipo</th>
                            <th>Data</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($lista_partidas as $linha): ?>
                        <tr>
                            <td><?php echo $linha->id_partida;?></td>
                            <td><?php echo $linha->nome_adversario;?></td>
                            <td><?php echo $linha->tipo_de_partida;?></td>
                            <td><?php echo $linha->data_partida;?></td>
                            <td>
                                <?php echo anchor('partidas/editar/'.$linha->id_partida, '<i class="fa fa-pencil"></i> Editar', array('class'=>'btn btn-warning btn-xs'));?>
                                <?php echo anchor('partidas/excluir/'.$linha->id_partida, '<i class="fa fa-trash"></i> Excluir', array('class'=>'btn btn-danger btn-xs'));?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
