<div class="panel panel-default panel-custom">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 colsm-12 col-xs-12">
                <?php
                /* ADVERSARIO */
                echo form_label('Adversário');
                echo form_dropdown('adversario_id', $lista_adversario, isset($busca) ? $busca->adversario_id : '', array('class'=>'form-control selectpicker','data-show-subtext'=>'true', 'data-live-search'=>'true','data-size'=>'10'));
                /* ADVERSARIO */
                /* CAMPEONATO */
                echo form_label('Tipo de partida');
                echo form_dropdown('id_tipo_de_partida', $lista_tipos_de_partidas, isset($busca) ? $busca->tipo_de_partida_id : '', array('class'=>'form-control selectpicker','data-show-subtext'=>'true', 'data-live-search'=>'true','data-size'=>'10'));
                /* CAMPEONATO */
                /* DATA DA PARTIDA */
                echo form_label('Data da partida');
                echo form_input(array('name'=>'data_partida', 'id'=>'data_partida', 'class'=>'form-control'), set_value('data_partida', isset($busca) ? $busca->data_partida : ''));
                /* DATA DA PARTIDA */
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php echo form_hidden('id', isset($busca) ? $busca->id_partida : '');?>
                <?php echo form_button(array(
                    'class' => 'btn btn-success',
                    'content' => '<span><i class="fa fa-floppy-o" aria-hidden="true"></i></span><span>Salvar</span>',
                    'type' => 'submit',
                ));?>
                <?php echo anchor('partidas', '<span><i class="fa fa-arrow-left" aria-hidden="true"></i></span><span>Voltar</span>', array('class' => 'btn btn-default'));?>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>
