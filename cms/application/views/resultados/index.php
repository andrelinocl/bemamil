<main>
    <div class="row">
        <div class="col-lg-12 bg--branco">
            <div class="page-header">
                <div class="row">
                    <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                        <h1>Notícias cadastradas</h1>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                        <?php echo anchor('resultados/adicionar', 'partidas', array('class'=>'btn btn-primary btn-block'));?>
                    </div>
                </div>
                <?php echo create_breadcrumb();?>
            </div>
            <div class="page-content">
                <?php echo isset($_SESSION['item']) ? $_SESSION['item'] : ''; ?>
                <table class="table table-responsive" id="lista-noticias">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Status</th>
                            <th>Tipo</th>
                            <th>Partida</th>
                            <th>Disputa Penailtis</th>
                            <th>Data</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($lista_partidas as $linha): ?>
                        <tr>
                            <td><?php echo $linha->id_partida;?></td>
                            <td>CRIAR STATUS</td>
                            <td><?php echo $linha->tipo_de_partida;?></td>
                            <td><?php echo 'Bem Amil FC '. $linha->placar_time .' x ' . $linha->placar_adversario . ' '. $linha->nome_adversario ;?></td>
                            <?php if($linha->resultado_penaltis != ''):?>
                            <td><?php echo $linha->resultado_penaltis;?></td>
                            <?php else:?>
                            <td>Não houve penalidades</td>
                            <?php endif;?>
                            <td><?php echo $linha->data_partida .' - '. $linha->horario_partida;?></td>
                            <td>
                                <?php echo anchor('resultados/editar/'.$linha->id_partida, '<i class="fa fa-pencil"></i> Editar', array('class'=>'btn btn-warning btn-xs'));?>
                                <?php echo anchor('partidas/excluir/'.$linha->id_partida, '<i class="fa fa-trash"></i> Excluir', array('class'=>'btn btn-danger btn-xs'));?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
