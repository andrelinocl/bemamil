<div class="panel panel-default panel-custom">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="row">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-6 text-right" style="padding-top: 20px;">
                                <?php echo heading('Bem Amil FC',4);?>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                <?php
                                /* PLACAR DO TIME */
                                echo form_label('Placar');
                                echo form_input(array('name'=>'placar_time', 'id'=>'placar_time', 'class'=>'form-control'), set_value('placar_time', isset($busca) ? $busca->placar_time : ''));
                                /* PLACAR DO TIME */
                                 ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                <?php
                                /* PLACAR ADVERSARIO */
                                echo form_label('Placar');
                                echo form_input(array('name'=>'placar_adversario', 'id'=>'placar_adversario', 'class'=>'form-control'), set_value('placar_adversario', isset($busca) ? $busca->placar_adversario : ''));
                                /* PLACAR ADVERSARIO */
                                ?>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-6">
                                <?php
                                /* ADVERSARIO */
                                echo form_label('Adversário');
                                echo form_dropdown('adversario_id', $lista_adversario, isset($busca) ? $busca->adversario_id : '', array('class'=>'form-control selectpicker','data-show-subtext'=>'true', 'data-live-search'=>'true','data-size'=>'10'));
                                /* ADVERSARIO */
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <?php
                                /* CAMPEONATO */
                                echo form_label('Tipo de partida');
                                echo form_dropdown('id_tipo_de_partida', $lista_tipos_de_partidas, isset($busca) ? $busca->tipo_de_partida_id : '', array('class'=>'form-control selectpicker','data-show-subtext'=>'true', 'data-live-search'=>'true','data-size'=>'10'));
                                /* CAMPEONATO */
                                /* LOCAL DA PARTIDA */
                                echo form_label('Local');
                                echo form_input(array('name'=>'local', 'id'=>'local', 'class'=>'form-control'), set_value('local', isset($busca) ? $busca->local : ''));
                                /* LOCAL DA PARTIDA */
                                /* DATA DA PARTIDA */
                                echo form_label('Data');
                                echo form_input(array('name'=>'data_partida', 'id'=>'data_partida', 'class'=>'form-control'), set_value('data_partida', isset($busca) ? $busca->data_partida : ''));
                                /* DATA DA PARTIDA */
                                /* DATA DA PARTIDA */
                                echo form_label('Horário');
                                echo form_input(array('name'=>'horario_partida', 'id'=>'horario_partida', 'class'=>'form-control'), set_value('horario_partida', isset($busca) ? $busca->horario_partida : ''));
                                /* DATA DA PARTIDA */
                                ?>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <?php
                                /* PLACAR ADVERSARIO */
                                echo form_label('Se houve penalidades após o término da partida', '',array('class'=>'thin'));
                                echo form_input(array('name'=>'resultado_penaltis', 'id'=>'resultado_penaltis', 'class'=>'form-control', 'placeholder'=>'Exemplo: 5x3'), set_value('resultado_penaltis', isset($busca) ? $busca->resultado_penaltis : ''));
                                /* PLACAR ADVERSARIO */
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php echo form_hidden('id', isset($busca) ? $busca->id_partida : '');?>
                <?php echo form_button(array(
                    'class' => 'btn btn-success',
                    'content' => '<span><i class="fa fa-floppy-o" aria-hidden="true"></i></span><span>Salvar</span>',
                    'type' => 'submit',
                ));?>
                <?php echo anchor('partidas', '<span><i class="fa fa-arrow-left" aria-hidden="true"></i></span><span>Voltar</span>', array('class' => 'btn btn-default'));?>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>
