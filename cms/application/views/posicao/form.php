<div class="panel panel-default panel-custom">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php
                    /* POSICAO */
                    echo form_label('Posicao')."\r\n";
                    echo form_input(array('name'=>'posicao', 'id'=>'posicao', 'class'=>'form-control'),set_value('posicao', isset($busca) ? $busca->posicao : ''));
                    echo form_error('nome', '<div class="alert alert-warning" role="alert">','</div>');
                    /* POSICAO */
                    ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php echo form_hidden('id', isset($busca) ? $busca->id : '');?>
                <?php echo form_button(array(
                    'class' => 'btn btn-success',
                    'content' => '<span><i class="fa fa-floppy-o" aria-hidden="true"></i></span><span>Salvar</span>',
                    'type' => 'submit',
                ));?>
                <?php echo anchor('noticias', '<span><i class="fa fa-arrow-left" aria-hidden="true"></i></span><span>Voltar</span>', array('class' => 'btn btn-default'));?>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>
