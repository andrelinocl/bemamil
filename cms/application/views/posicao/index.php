<main>
    <div class="row">
        <div class="col-lg-12 bg--branco">
            <div class="page-header">
                <div class="row">
                    <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                        <h1>Posições Disponíveis</h1>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                        <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target="#addPosicao">Adicionar uma posição</a>
                    </div>
                </div>
                <?php echo create_breadcrumb();?>
            </div>
            <div class="page-content">
                <?php echo isset($_SESSION['item']) ? $_SESSION['item'] : ''; ?>
                <table class="table table-responsive" id="lista-noticias">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Posição</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $var = '';?>
                        <?php foreach($lista_posicao as $linha): ?>
                        <tr>
                            <td><?php echo $linha->id_posicao;?></td>
                            <td>
                                <?php echo $linha->posicao;?>
                            </td>
                            <td>
                                <a href="#" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#addPosicao"><i class="fa fa-pencil"></i> Editar</a>
                                <?php  anchor('noticias/editar/'.$linha->id_posicao, '<i class="fa fa-pencil"></i> Editar', array('class'=>'btn btn-warning btn-xs'));?>
                                <?php  anchor('noticias/excluir/'.$linha->id_posicao, '<i class="fa fa-trash"></i> Excluir', array('class'=>'btn btn-danger btn-xs'));?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
<div id="addPosicao" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Adicionar Posição</h4>
            </div>
            <div class="modal-body">
                <?php
                echo form_open_multipart('posicao/store', array('class'=>'form'));
                /* POSICAO */
                echo form_label('Para adicionar um posição preencha o campo abaixo')."\r\n";
                echo form_input(array('name'=>'posicao', 'id'=>'posicao', 'class'=>'form-control'),set_value('posicao', isset($busca) ? $busca->posicao : ''));
                echo form_error('nome', '<div class="alert alert-warning" role="alert">','</div>');
                /* POSICAO */

                echo form_hidden('id', isset($busca) ? $busca->id : '');
                echo form_button(array(
                    'class' => 'btn btn-success',
                    'content' => '<span><i class="fa fa-floppy-o" aria-hidden="true"></i></span><span>Salvar</span>',
                    'type' => 'submit',
                ));
                echo form_close();?>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
