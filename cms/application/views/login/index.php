<?php if(!$this->session->userdata("usuario_logado")) : ?>
<div class="wrapper">
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <!--i class="fa fa-5x fa-lock" aria-hidden="true"></i-->
                <img src="<?php echo base_url('assets/icons/locked.png');?> " alt="" />
                <h3 class="panel-title">Login</h3>
            </div>
            <div class="panel-body">
                <?php
                    echo form_open("usuarios/login");
                    echo '<div class="form-group">';
                    echo form_input(array(
                            'name' => 'login',
                            'id' => 'login',
                            'class' => 'form-control',
                            'maxlenth' => '255',
                            'placeholder'=>'Login',
                            'autofocus'=>'autofocus',
                        ), set_value('login'));
                    echo form_error('login', '<div class="alert alert-warning" role="alert">','</div>');
                    echo '</div>';
                    echo '<div class="form-group">';
                    echo form_password(array(
                            'name' => 'senha',
                            'id' => 'senha',
                            'class' => 'form-control',
                            'maxlenth' => '255',
                            'placeholder'=>'Senha',
                        ));
                    echo form_error('senha','<div class="alert alert-warning" role="alert">', '</div>');
                    echo '</div>';
                    echo form_button(array(
                        'class' => 'btn btn-primary btn-login outline pull-right',
                        'content' => '<span><i class="fa fa-arrow-right" aria-hidden="true"></i></span><span>Login</span>',
                        'type' => 'submit',
                    ));
                    echo form_close();
                ?>
            </div>
        </div>
        <a href="javascript:void(0)" class="link">Esqueceu sua Senha?</a>
    </div>
    <div id="particles-js"></div>
</div>
<?php endif; ?>
