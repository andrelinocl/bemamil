<aside>
    <div class="user">
        <figure>
            <img class="img-responsive img-circle" src="<?php echo base_url('assets/imagem/no-user.jpg'); ?>" alt="" />
        </figure>
        <span>Bem Vindo<br><strong>Allan Dellon</strong></span>
        <hr>
    </div>
    <nav>
        <ul class="nav metismenu" id="menu">
            <li class="active">
                <a href="#" aria-expanded="true">
                    <span class="fa fa-newspaper-o fa-lg"></span>
                    <span class="">Notícias</span>
                    <span class="fa arrow fa-fw"></span>
                </a>
                <ul aria-expanded="true" class="nav">
                    <li>
                        <a href="<?php echo base_url('noticias');?>">
                            <span class="fa fa-plus fa-fw"></span>
                            Notícia
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#" aria-expanded="false">Time<span class="fa arrow fa-fw"></span></a>
                <ul aria-expanded="false" class="nav">
                    <li><a href="<?php echo base_url('elenco');?>">Elenco</a></li>
                    <li><a href="<?php echo base_url('posicao');?>">Posições</a></li>
                    <li><a href="#">História</a></li>
                </ul>
            </li>
            <li>
                <a href="#" aria-expanded="false">Jogos<span class="fa arrow fa-fw"></span></a>
                <ul aria-expanded="false" class="nav">
                    <li><a href="<?php echo base_url('partidas');?>">Partidas</a></li>
                    <li><a href="<?php echo base_url('adversarios');?>">Adversários</a></li>
                </ul>
            </li>
            <li><a href="<?php echo base_url('resultados');?>">Resultados</a></li>
        </ul>
    </nav>
</aside>
