<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="<?php echo base_url('assets/imagens/logo-bem-amil-24.png');?>" />

	<title><?php echo $title; ?></title>

	<?php echo link_tag('assets/font-awesome/css/font-awesome.min.css'); ?>
	<?php echo link_tag('assets/css/bootstrap.css'); ?>
	<?php echo link_tag('assets/lib/bootstrap-datepicker/css/bootstrap-datepicker3.css');?>
	<?php echo link_tag('assets/css/metis-menu.css');?>
	<?php echo link_tag('assets/css/datatable.css');?>
	<?php echo link_tag('assets/summernote-master/dist/summernote.css');?>
	<?php echo link_tag('assets/css/bootstrap-select.min.css');?>
	<?php echo link_tag('assets/css/main.css'); ?>

</head>
<body<?php echo !$this->session->userdata('logado') ? ' class="login"' : ''; ?>>

	<?php if($this->session->userdata('logado')):?>
	<nav class="navbar navbar-default navbar-static-top">
	    <div class="container-fluid">
	        <div class="navbar-header">
	            <button type="button" class="sidebar-hide-box">
	                <i class="fa fa-bars"></i>
	            </button>
	        </div>
	        <div class="navbar-links">
	            <ul class="nav navbar-nav navbar-right">
	                <li><a href="<?php echo base_url('usuarios/logout');?>"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li>
	            </ul>
	        </div>
	    </div>
	</nav>
	<?php endif;?>
