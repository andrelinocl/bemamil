

    <?php echo script_tag('assets/js/jquery-2.2.4.min.js'); ?>
    <?php echo script_tag('assets/js/bootstrap.min.js'); ?>
    <?php echo script_tag('assets/js/particles.min.js');?>
    <?php echo script_tag('assets/js/metis-menu.js');?>
    <?php echo script_tag('assets/js/datatable.js');?>
    <?php echo script_tag('assets/js/datatable-bootstrap.js');?>
    <?php echo script_tag('assets/js/jQuery.stringToSlug.js');?>
    <?php echo script_tag('assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.min.js');?>
    <?php echo script_tag('assets/lib/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js');?>
    <?php echo script_tag('assets/summernote-master/dist/summernote.min.js');?>
    <?php echo script_tag('assets/summernote-master/lang/summernote-pt-BR.js');?>
    <?php echo script_tag('assets/lib/tinymce/tinymce.min.js');?>
    <?php echo script_tag('assets/js/bootstrap-select.min.js');?>
    <?php echo script_tag('assets/js/main.js'); ?>
</body>
</html>
