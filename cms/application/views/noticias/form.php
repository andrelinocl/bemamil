<div class="panel panel-default panel-custom">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 colsm-12 col-xs-12">
                <?php echo form_label('Titulo')."\r\n";?>
                <?php echo form_input(array('name'=>'titulo', 'id'=>'titulo', 'class'=>'form-control'),set_value('titulo', isset($busca) ? $busca->titulo : ''));?>
                <?php echo form_error('titulo', '<div class="alert alert-warning" role="alert">','</div>');?>
                <?php echo form_label('Slug')."\r\n";?>
                <?php echo form_input(array('name'=>'slug', 'id'=>'slug', 'class'=>'form-control', 'disabled'=>'disabled'), set_value('slug', isset($busca) ? $busca->slug : ''));?>
                <div class="row">
                    <div class="col-lg-6">
                        <?php echo form_label('Data de publicação')."\r\n";?>
                        <?php echo form_input(array('name'=>'data_publicacao', 'id'=>'data_publicacao', 'class'=>'form-control'), set_value('data_publicacao', isset($busca) ? $busca->data_publicacao : ''));?>
                        <?php echo form_error('data_publicacao', '<div class="alert alert-warning" role="alert">','</div>');?>
                        <?php echo form_label('Imagem')."\r\n";?>
                        <?php echo form_upload(array('name'=>'imagem', 'id'=>'imgInp', 'class'=>'form-control'), set_value('imagem', isset($busca) ? $busca->imagem : ''));?>
                        <?php echo form_error('imagem', '<div class="alert alert-warning" role="alert">','</div>');?>
                    </div>
                    <div class="col-lg-6">
                        <label for="imgOut">Preview <small class="thin">(Imagem de destaque da notícia)</small></label>
                        <div class="img-thumb">
                            <img src="<?php echo isset($busca) ? '/bemamil/uploads/'.$busca->imagem : '' ;?>" alt="" id="imgOut"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?php echo form_label('Resumo <small class="thin">(Aparecerá no slider da home)</small>')."\r\n";?>
                        <?php echo form_textarea(array('name'=>'resumo', 'id'=>'summernote', 'class'=>'form-control summernote-textarea-resumo'), set_value('resumo', isset($busca) ? $busca->resumo : ''));?>
                        <?php echo form_error('resumo', '<div class="alert alert-warning" role="alert">','</div>');?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?php echo form_label('Texto')."\r\n";?>
                        <?php echo form_textarea(array('name'=>'texto', 'id'=>'', 'class'=>'form-control summernote-textarea'), set_value('texto', isset($busca) ? $busca->texto : ''));?>
                        <?php echo form_error('texto', '<div class="alert alert-warning" role="alert">','</div>');?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php echo form_hidden('id', isset($busca) ? $busca->id : '');?>
                <?php echo form_button(array(
                    'class' => 'btn btn-success',
                    'content' => '<span><i class="fa fa-floppy-o" aria-hidden="true"></i></span><span>Salvar</span>',
                    'type' => 'submit',
                ));?>
                <?php echo anchor('noticias', '<span><i class="fa fa-arrow-left" aria-hidden="true"></i></span><span>Voltar</span>', array('class' => 'btn btn-default'));?>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>
