<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Elenco extends MY_Controller {

	private $title = 'FRAGIO CMS';

	/* VALIDACAO DO FORMULARIO */
    private $validate = array(
        array(
            'field' => 'nome',
            'label' => 'Nome',
            'rules' => 'trim|required',
			'errors' => array(
                'required' => 'Este campo é obrigatório',
            ),
        ),
		array(
			'field' => 'camisa',
			'label' => 'Camisa',
			'rules' => 'trim|required',
			'errors' => array(
                'required' => 'Este campo é obrigatório',
            ),
		),
        array(
            'field' => 'posicao_id',
            'label' => 'Posição',
            'rules' => 'trim|required',
			'errors' => array(
                'required' => 'Este campo é obrigatório',
            ),
        )
    );

	/* CLASSE CONSTRUTORA */
	public function __construct(){
		parent::__construct();

		$this->load->model('elenco_model');
		$this->load->library('form_validation');
		$this->load->helper('breadcrumb');
		$this->load->helper('message');
	}


    /* FUNCOES DE CARREGAMENTO DAS VIEWS */
	public function index(){
		$this->data += array(
			'title' => $this->title,
			'lista_elenco' => $this->elenco_model->buscar_todos(),
		);
		$this->load->view('template', $this->data);
	}

	public function adicionar(){
		$this->data += array(
			'title' => $this->title,
			'lista_posicao' => $this->elenco_model->busca_posicao(),
		);
		$this->load->view('template', $this->data);
	}

	public function editar(){
		$this->data += array(
			'title' => $this->title,
			'lista_posicao' => $this->elenco_model->busca_posicao(),
		);
		$this->load->view('template', $this->data);
	}

	public function excluir($slug){
		$this->delete($slug);
	}



	/* FUNCOES PARA MANUPULACAO DO BANCO */
	public function store(){

		$this->form_validation->set_rules($this->validate);
		if($this->form_validation->run() == TRUE){

			$this->do_upload('imagem');
			$dados = array(
				'nome'            => $this->input->post('nome'),
				'apelido'         => $this->input->post('apelido'),
				'camisa'          => $this->input->post('camisa'),
				'slug'            => strtolower(url_title($this->input->post('nome'))),
				'posicao_id'      => $this->input->post('posicao_id'),
				'imagem'          => $this->upload->data('file_name'),
				'texto'           => $this->input->post('texto'),
			);

//			die(print_r($dados));
			$result = $this->elenco_model->adicionar($dados);
			if($result){
				$this->session->set_flashdata('item', create_message('success', 'thumbs-up', 'Registro Salvo com sucesso'));
			}else{
				$this->session->set_flashdata('item', create_message('danger', 'thumbs-down', 'O Registro não pode ser salvo. Tente novamente!'));
			}
			redirect($this->router->fetch_class());
		}
	}


	public function update($id){

		$this->form_validation->set_rules($this->validate);
		if($this->form_validation->run() == TRUE):

			$this->do_upload('imagem');
			$dados = array(
				'nome'            => $this->input->post('nome'),
				'apelido'         => $this->input->post('apelido'),
				'camisa'          => $this->input->post('camisa'),
				'slug'            => strtolower(url_title($this->input->post('nome'))),
				'posicao_id'      => $this->input->post('posicao_id'),
				'imagem'          => $this->upload->data('file_name'),
				'texto'           => $this->input->post('texto'),
			);
			$result = $this->elenco_model->editar($dados, $id);
			if($result){
				$this->session->set_flashdata('item', create_message('success', 'thumbs-up', 'Registro Salvo com sucesso'));
			}else{
				$this->session->set_flashdata('item', create_message('danger', 'thumbs-down', 'O Registro não pode ser salvo. Tente novamente!'));
			}
			redirect('elenco');
		endif;
	}

	public function delete($id){
		if(!$id || !$this->elenco_model->excluir($id))
			$this->session->set_flashdata('item', create_message('danger', 'thumbs-down', 'O Registro não pode ser excluído. Tente novamente!'));
		else
			$this->session->set_flashdata('item', create_message('success', 'thumbs-up', 'Registro excluído com sucesso.'));

		redirect('elenco');
	}

}
