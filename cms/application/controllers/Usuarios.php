<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller{

    private $validate = array(
        array(
            'field' => 'login',
            'label' => 'Login',
            'rules' => 'trim|required|min_length[3]|max_length[20]|alpha_numeric',
            'errors' => array(
                'required' => 'Você precisa informar seu %s.',
                'min_length' => 'Seu %s precisa ter no mínimo 3 caracteres',
                'max_length' => 'Seu %s precisa ter no máxino 20 caracteres',
                'alpha_numeric' => 'São aceitos apenas caracteres alpha numéricos',
            ),
        ),
        array(
            'field' => 'senha',
            'label' => 'Senha',
            'rules' => 'trim|required|min_length[3]',
            'errors'=> array(
                'required' => 'Você precisa informar sua %s.',
                'min_length' => 'Sua %s precisa ter no mínimo 3 caracteres',
            ),
        ),
        //$this->form_validation->set_rules('field_name', 'Field Label', 'rule1|rule2|rule3',
        //array('rule2' => 'Error Message on rule2 for this field_name'));
    );

    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $this->login();
    }


    public function login(){

        $this->load->library('form_validation');

        $data['title'] = 'FRAGIOCMS';

        $this->form_validation->set_rules($this->validate);

        if($this->form_validation->run()){

            $login = $this->input->post('login');
            $senha = $this->input->post('senha');

            $this->load->model('usuarios_model');
            $verifica_acesso = $this->usuarios_model->acesso($login, $senha);

            if($verifica_acesso === TRUE){
                $this->session->set_userdata('logado', TRUE);
                redirect(base_url('home'));
            }
        }

        $this->load->view('template/header', $data);
        $this->load->view('login/index');
        $this->load->view('template/footer');

    }

    public function logout(){
        $this->session->sess_destroy();
        redirect('usuarios/login');
    }

    function hash_password($pass){
        $key = $this->config->item('encryption_key');
        $salt1 = hash('sha512', $key . $pass);
        $salt2 = hash('sha512', $pass . $key);
        $hashed_password = hash('sha512', $salt1 . $pass . $salt2);

        return $hashed_password;
    }
}
