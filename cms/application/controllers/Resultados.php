<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resultados extends MY_Controller {

	private $title = 'FRAGIO CMS';

	/* VALIDACAO DO FORMULARIO */
    private $validate = array(
        array(
            'field' => 'placar_time',
            'label' => 'Placar do Time',
            'rules' => 'trim|required',
			'errors' => array(
                'required' => 'Este campo é obrigatório',
            ),
        ),
		array(
			'field' => 'placar_adversario',
            'label' => 'Placar do Adversário',
            'rules' => 'trim|required',
			'errors' => array(
                'required' => 'Este campo é obrigatório',
            ),
		),
		array(
			'field' => 'adversario_id',
            'label' => 'Time Adversário',
            'rules' => 'trim|required',
			'errors' => array(
                'required' => 'Este campo é obrigatório',
            ),
		),
		array(
			'field' => 'id_tipo_de_partida',
            'label' => 'Tipo de Partida',
            'rules' => 'trim|required',
			'errors' => array(
                'required' => 'Este campo é obrigatório',
            ),
		),
		array(
			'field' => 'local',
            'label' => 'Local',
            'rules' => 'trim|required',
			'errors' => array(
                'required' => 'Este campo é obrigatório',
            ),
		),
		array(
			'field' => 'data_partida',
            'label' => 'Data',
            'rules' => 'trim|required',
			'errors' => array(
                'required' => 'Este campo é obrigatório',
            ),
		),
    );

	/* CLASSE CONSTRUTORA */
	public function __construct(){
		parent::__construct();

		$this->load->model('resultados_model');
		$this->load->model('partidas_model');
		$this->load->library('form_validation');
		$this->load->helper('breadcrumb');
		$this->load->helper('message');
	}


    /* FUNCOES DE CARREGAMENTO DAS VIEWS */
	public function index(){
		$this->data += array(
			'title' => $this->title,
			'lista_partidas' => $this->partidas_model->buscar_todas(),
		);
		$this->load->view('template', $this->data);
	}

	public function adicionar(){
		$this->data += array(
			'title' => $this->title,
			'lista_adversario' => $this->partidas_model->busca_adversario(),
			'lista_tipos_de_partidas' => $this->partidas_model->busca_tipos_de_partidas(),
		);
		$this->load->view('template', $this->data);
	}

	public function editar(){
		$this->data += array(
			'title' => $this->title,
			'lista_adversario' => $this->partidas_model->busca_adversario(),
			'lista_tipos_de_partidas' => $this->partidas_model->busca_tipos_de_partidas(),
		);
		$this->load->view('template', $this->data);
	}

	public function excluir($slug){
		$this->delete($slug);
	}



	/* FUNCOES PARA MANUPULACAO DO BANCO */
	public function store(){

		$this->form_validation->set_rules($this->validate);
		if($this->form_validation->run() == TRUE){

			$dados = array(
				'placar_time' => $this->input->post('placar_time'),
				'placar_adversario' => $this->input->post('placar_adversario'),
				'adversario_id' => $this->input->post('adversario_id'),
				'tipo_de_partida_id' => $this->input->post('id_tipo_de_partida'),
				'local' => $this->input->post('local'),
				'data_partida' => $this->input->post('data_partida'),
				'horario_partida' => $this->input->post('horario_partida'),
				'resultado_penaltis' => $this->input->post('resultado_penaltis'),
			);
			//echo '<pre>';
			//die(print_r($dados));
			$result = $this->partidas_model->adicionar($dados);
			if($result){
				$this->session->set_flashdata('item', create_message('success', 'thumbs-up', 'Registro Salvo com sucesso'));
			}else{
				$this->session->set_flashdata('item', create_message('danger', 'thumbs-down', 'O Registro não pode ser salvo. Tente novamente!'));
			}
			redirect($this->router->fetch_class());
		}
	}


	public function update($id){

		$this->form_validation->set_rules($this->validate);
		if($this->form_validation->run() == TRUE):

			$dados = array(
				'placar_time' => $this->input->post('placar_time'),
				'placar_adversario' => $this->input->post('placar_adversario'),
				'adversario_id' => $this->input->post('adversario_id'),
				'tipo_de_partida_id' => $this->input->post('id_tipo_de_partida'),
				'local' => $this->input->post('local'),
				'data_partida' => $this->input->post('data_partida'),
				'horario_partida' => $this->input->post('horario_partida'),
				'resultado_penaltis' => $this->input->post('resultado_penaltis'),
			);
			//die(print_r($dados));
			$result = $this->partidas_model->editar($dados, $id);
			if($result){
				$this->session->set_flashdata('item', create_message('success', 'thumbs-up', 'Registro Salvo com sucesso'));
			}else{
				$this->session->set_flashdata('item', create_message('danger', 'thumbs-down', 'O Registro não pode ser salvo. Tente novamente!'));
			}
			redirect($this->router->fetch_class());
		endif;
	}
/*
	public function delete($slug){
		if(!$slug || !$this->noticias_model->excluir($slug))
			$this->session->set_flashdata('item', create_message('danger', 'thumbs-down', 'O Registro não pode ser excluído. Tente novamente!'));
		else
			$this->session->set_flashdata('item', create_message('success', 'thumbs-up', 'Registro excluído com sucesso.'));

		redirect('noticias');
	}
*/
}
