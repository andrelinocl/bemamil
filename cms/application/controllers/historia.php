<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Historia extends MY_Controller {

	private $title = 'FRAGIO CMS';

	/* VALIDACAO DO FORMULARIO */
    private $validate = array(
        array(
            'field' => 'texto',
            'label' => 'Texto',
            'rules' => 'trim|required',
			'errors' => array(
                'required' => 'Este campo é obrigatório',
            ),
        )
    );

	/* CLASSE CONSTRUTORA */
	public function __construct(){
		parent::__construct();

		$this->load->model('noticias_model');
		$this->load->library('form_validation');
		$this->load->helper('breadcrumb');
		$this->load->helper('message');
	}


    /* FUNCOES DE CARREGAMENTO DAS VIEWS */
	public function index(){
		$this->data += array(
			'title' => $this->title,
			'lista_noticias' => $this->noticias_model->buscar_todas(),
		);
		$this->load->view('template', $this->data);
	}

	public function adicionar(){
		$this->data += array(
			'title' => $this->title,
		);
		$this->load->view('template', $this->data);
	}

	public function editar(){
		$this->data += array(
			'title' => $this->title,
		);
		$this->load->view('template', $this->data);
	}

	public function excluir($slug){
		$this->delete($slug);
	}



	/* FUNCOES PARA MANUPULACAO DO BANCO */
	public function store(){

		$this->form_validation->set_rules($this->validate);
		if($this->form_validation->run() == TRUE){

			$this->do_upload('imagem');
			$dados = array(
				'titulo'            => $this->input->post('titulo'),
				'data_publicacao'   => $this->input->post('data_publicacao'),
				'texto'             => $this->input->post('texto'),
				'resumo'             => $this->input->post('resumo'),
				'slug'              => url_title(strtolower($this->input->post('titulo'))),
				'imagem'            => $this->upload->data('file_name'),
			);
			$result = $this->noticias_model->adicionar($dados);
			if($result){
				$this->session->set_flashdata('item', create_message('success', 'thumbs-up', 'Registro Salvo com sucesso'));
			}else{
				$this->session->set_flashdata('item', create_message('danger', 'thumbs-down', 'O Registro não pode ser salvo. Tente novamente!'));
			}
			redirect($this->router->fetch_class());
		}
	}


	public function update($id){

		$this->form_validation->set_rules($this->validate);
		if($this->form_validation->run() == TRUE):

			$this->do_upload('imagem');
			$dados = array(
				'titulo'            => $this->input->post('titulo'),
				'data_publicacao'   => $this->input->post('data_publicacao'),
				'texto'             => $this->input->post('texto'),
				'resumo'            => $this->input->post('resumo'),
				'slug'              => url_title(strtolower($this->input->post('titulo'))),
				'imagem'            => $this->upload->data('file_name'),
			);
			$result = $this->noticias_model->editar($dados, $id);
			if($result){
				$this->session->set_flashdata('item', create_message('success', 'thumbs-up', 'Registro Salvo com sucesso'));
			}else{
				$this->session->set_flashdata('item', create_message('danger', 'thumbs-down', 'O Registro não pode ser salvo. Tente novamente!'));
			}
			redirect('noticias');
		endif;
	}
/*
	public function delete($slug){
		if(!$slug || !$this->noticias_model->excluir($slug))
			$this->session->set_flashdata('item', create_message('danger', 'thumbs-down', 'O Registro não pode ser excluído. Tente novamente!'));
		else
			$this->session->set_flashdata('item', create_message('success', 'thumbs-up', 'Registro excluído com sucesso.'));

		redirect('noticias');
	}
*/
}
