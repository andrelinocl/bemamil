<?php defined('BASEPATH') OR exit('No direct script access allowed');

    Class MY_Controller extends CI_Controller{

        protected $data = array();

        public function __construct(){
            parent::__construct();
            $this->beforeLoader();
            $this->load->model('Usuarios_Model');
            $this->Usuarios_Model->logado();
        }

        public function beforeLoader () {
            $this->data += array(
                'controller' => $this->router->fetch_class(),
    			'action' => $this->router->fetch_method()
            );
        }

        public function do_upload(){
            $config['upload_path'] = '../uploads';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '2048';
            $config['max_width'] = '1920';
            $config['max_height'] = '1080';
            $config['remove_spaces'] = true;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('imagem'))
            {
                $error = array('error' => $this->upload->display_errors());
            }
            else
            {
                $data = array('upload_data' => $this->upload->data());
            }
        }
    }
