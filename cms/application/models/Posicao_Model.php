<?php

    class Posicao_Model extends CI_Model{

        private $tabela = 'posicao';

        public function buscar_todos(){
            $busca = $this->db->get($this->tabela);
            return $busca->result();
        }

        public function buscar_pelo_id($id = NULL){
            $this->db->where('id', $id);
            $this->db->limit(1);
            return $this->db->get($this->tabela);
        }

        public function adicionar($dados = NULL){
            $this->db->insert($this->tabela, $dados);
            if($this->db->affected_rows() > 0){
                return TRUE;
            }else{
                return FALSE;
            }
        }

    }
