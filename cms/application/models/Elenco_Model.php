<?php

    class Elenco_Model extends CI_Model{

        private $tabela = 'elenco';

        public function buscar_todos(){
            $this->db->join('posicao', 'posicao.id_posicao = '.$this->tabela.'.posicao_id');
            $busca = $this->db->get($this->tabela);
            return $busca->result();
        }

        public function busca_posicao(){
            $this->db->from('posicao');
            $this->db->order_by('id_posicao');
            $result = $this->db->get();
            $busca = array();
            if($result->num_rows() > 0) {
                foreach($result->result_array() as $row) {
                    $busca[$row['id_posicao']] = $row['posicao'];
                }
            }
            return $busca;
        }

        public function buscar_pela_slug($slug = NULL){
            $this->db->where('slug', $slug);
            $this->db->limit(1);
            return $this->db->get($this->tabela);
        }

        public function adicionar($dados = NULL){
            $this->db->insert($this->tabela, $dados);
            if($this->db->affected_rows() > 0){
                return TRUE;
            }else{
                return FALSE;
            }
        }

        public function editar($dados = NULL, $id = NULL){
            $this->db->where('id', $id);
            $this->db->update($this->tabela, $dados);
            if($this->db->affected_rows() > 0){
                return TRUE;
            }else{
                return FALSE;
            }
        }

        public function excluir($id = NULL){
            $this->db->where('id', $id);
            return $this->db->delete($this->tabela);
        }
    }
