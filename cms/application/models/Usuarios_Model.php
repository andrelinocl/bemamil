<?php

class Usuarios_Model extends CI_Model{

    private $tabela = 'usuarios';

    public function acesso($login = NULL, $senha = NULL){
		if( !$login && !$senha ):
			return false;
		else:
			$this->db->where(
                array(
                    'login'=>$login,
                    'senha'=>md5($senha)
                )
            );
			$query = $this->db->get($this->tabela);
			if( $query->num_rows() == 1 ):
				return true;
			else:
				return false;
            endif;
		endif;
	}
/*
    public function acesso($login, $senha, $key){

        $salt1 = hash('sha512', $key . $senha);
        $salt2 = hash('sha512', $senha . $key);
        $pass = hash('sha512', $salt1 . $senha . $salt2);

        $this->db->where('login', $login);
        $this->db->where('senha', $pass);
        $this->db->limit(1);
        $query = $this->db->get($this->tabela);
        if ($query->num_rows == 1){
            return TRUE;
        }else{
            return FALSE;
        }

    }*/

	public function logado(){
		if( $this->session->userdata('logado') !== true ):
			redirect(site_url('usuarios/login'));
        endif;
	}
}
