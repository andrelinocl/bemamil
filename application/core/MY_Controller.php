<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	protected $data = array();

	public function __construct(){
		parent::__construct();
		$this->beforeLoader();
	}

	public function beforeLoader () {
		$this->data += array(
			'controller' => $this->router->fetch_class(),
			'action' => $this->router->fetch_method()
		);
	}
}
