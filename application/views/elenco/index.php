<section class="team">
    <div class="container no-padding">
        <h1>Nosso Time</h1>
        <div class="row no-margin">
            <div class="col-lg-3 no-padding">
                <div class="player">
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">
                        <figure>
                            <img src="<?php echo base_url('assets/imagens/jogadores/mattia-perin.jpg'); ?>" alt="" />
                        </figure>
                        <div class="player-info">
                            <big>12</big>
                            <h5>Mattia Perin</h5>
                            <p>Mais informações sobre o atleta</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 no-padding">
                <div class="player">
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">
                        <figure>
                            <img src="<?php echo base_url('assets/imagens/jogadores/Antonio-Rudiger.jpg'); ?>" alt="" />
                        </figure>
                        <div class="player-info">
                            <big>3</big>
                            <h5>Antonio Rudiguer</h5>
                            <p>Mais informações sobre o atleta</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 no-padding">
                <div class="player">
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">
                        <figure>
                            <img src="<?php echo base_url('assets/imagens/jogadores/sven-bender.jpg'); ?>" alt="" />
                        </figure>
                        <div class="player-info">
                            <big>5</big>
                            <h5>Sven Bender</h5>
                            <p>Mais informações sobre o atleta</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 no-padding">
                <div class="player">
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">
                        <figure>
                            <img src="<?php echo base_url('assets/imagens/jogadores/lionel-messi-2.jpg'); ?>" alt="" />
                        </figure>
                        <div class="player-info">
                            <big>10</big>
                            <h5>Lionel Messi</h5>
                            <p>Mais informações sobre o atleta</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="row no-margin">
            <div class="col-lg-3 no-padding">
                <div class="player">
                    <a href="javascript:void(0)"  data-toggle="modal" data-target="#myModal">
                        <figure>
                            <img src="<?php echo base_url('assets/imagens/jogadores/Gareth_Bale.jpg'); ?>" alt="" />
                        </figure>
                        <div class="player-info">
                            <big>11</big>
                            <h5>Gareth Bale</h5>
                            <p>Mais informações sobre o atleta</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 no-padding">
                <div class="player">
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">
                        <figure>
                            <img src="<?php echo base_url('assets/imagens/jogadores/luis-suarez.jpg'); ?>" alt="" />
                        </figure>
                        <div class="player-info">
                            <big>9</big>
                            <h5>Luis Suarez</h5>
                            <p>Mais informações sobre o atleta</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 no-padding">
                <div class="player">
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">
                        <figure>
                            <img src="<?php echo base_url('assets/imagens/jogadores/mattia-perin.jpg'); ?>" alt="" />
                        </figure>
                        <div class="player-info">
                            <big>12</big>
                            <h5>Mattia Perin</h5>
                            <p>Mais informações sobre o atleta</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 no-padding">
                <div class="player">
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">
                        <figure>
                            <img src="<?php echo base_url('assets/imagens/jogadores/Antonio-Rudiger.jpg'); ?>" alt="" />
                        </figure>
                        <div class="player-info">
                            <big>3</big>
                            <h5>Antonio Rudiguer</h5>
                            <p>Mais informações sobre o atleta</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="row no-margin">
            <div class="col-lg-3 no-padding">
                <div class="player">
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">
                        <figure>
                            <img src="<?php echo base_url('assets/imagens/jogadores/sven-bender.jpg'); ?>" alt="" />
                        </figure>
                        <div class="player-info">
                            <big>5</big>
                            <h5>Sven Bender</h5>
                            <p>Mais informações sobre o atleta</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 no-padding">
                <div class="player">
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">
                        <figure>
                            <img src="<?php echo base_url('assets/imagens/jogadores/lionel-messi-2.jpg'); ?>" alt="" />
                        </figure>
                        <div class="player-info">
                            <big>10</big>
                            <h5>Lionel Messi</h5>
                            <p>Mais informações sobre o atleta</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 no-padding">
                <div class="player">
                    <a href="javascript:void(0)"  data-toggle="modal" data-target="#myModal">
                        <figure>
                            <img src="<?php echo base_url('assets/imagens/jogadores/Gareth_Bale.jpg'); ?>" alt="" />
                        </figure>
                        <div class="player-info">
                            <big>11</big>
                            <h5>Gareth Bale</h5>
                            <p>Mais informações sobre o atleta</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 no-padding">
                <div class="player">
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">
                        <figure>
                            <img src="<?php echo base_url('assets/imagens/jogadores/luis-suarez.jpg'); ?>" alt="" />
                        </figure>
                        <div class="player-info">
                            <big>9</big>
                            <h5>Luis Suarez</h5>
                            <p>Mais informações sobre o atleta</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times"></i>
                </button>
                <h4 class="modal-title">Nome do jogador</h4>
            </div>
            <div class="modal-body no-padding">
                <div class="row no-margin">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 no-padding-left">
                        <div class="player">
                            <figure>
                                <img src="<?php echo base_url('assets/imagens/jogadores/mattia-perin.jpg'); ?>" alt="" />
                            </figure>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 no-padding">
                        <h5>Informações Pessoais</h5>
                        <ul>
                            <li>Nome completo: Mattia Perin</li>
                            <li>Data de nasc.: 10 de novembro de 1992 (23 anos)</li>
                            <li>Local de nasc.: Latina, Itália
                            <li>Altura: 1,88 m</li>
                            <li>Pé: Destro</li>
                        </ul>
                        <h5>Informações profissionais</h5>
                        <ul>
                            <li>Clube atual: Genoa</li>
                            <li>Número: 1</li>
                            <li>Posição: Goleiro</li>
                        </ul>
                            <h5>Clubes de juventude</h5>
                        <ul>
                            <li>Itália A.S.D. Pro Cisterna</li>
                            <li>Itália Pistoiese</li>
                            <li>Itália Genoa</li>
                        </ul>
                        <h5>Clubes profissionais2</h5>
                        <table>
                            <thead>
                                <tr>
                                    <th>Anos</th>
                                    <th>clubes</th>
                                    <th>Jogos e gols</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>2010-2011</td>
                                    <td>Itália Padova (emp.)</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>2011-2012</td>
                                    <td>Itália Pescara (emp.)</td>
                                    <td>95 (0)</td>
                                </tr>
                                <tr>
                                    <td>2012-</td>
                                    <td>Genoa</td>
                                    <td>25 (0)</td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <thead>
                                <tr>
                                    <th>Anos</th>
                                    <th>clubes</th>
                                    <th>Jogos e gols</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>2010-2011</td>
                                    <td>Itália Padova (emp.)</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>2011-2012</td>
                                    <td>Itália Pescara (emp.)</td>
                                    <td>95 (0)</td>
                                </tr>
                                <tr>
                                    <td>2012-</td>
                                    <td>Genoa</td>
                                    <td>25 (0)</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-md-offset-1 col-lg-3 col-lg-offset-1">
                        <h5>Descrição</h5>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
