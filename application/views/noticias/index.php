<?php $count = 0;?>
<section class="news">
    <div class="container">
        <h1>Notícias</h1>
        <?php foreach($buscar_todas as $linha):?>
        <?php if($count%2 == 0):?>
        <div class="row no-margin">
            <div class="col-lg-12 no-padding">
                <article class="article">
                    <div class="row no-margin">
                        <div class="col-lg-4 no-padding">
                            <figure>
                                <img src="<?php echo base_url('uploads/'.$linha->imagem); ?>" alt="<?php echo $linha->titulo;?>" />
                            </figure>
                        </div>
                        <div class="col-lg-8">
                            <small>publicado em: <?php echo $linha->data_publicacao ?></small>
                            <h3><?php echo $linha->titulo;?></h3>
                            <p>
                                <?php
                                echo $txt = character_limiter($linha->resumo, 1000, '...');
                                ?>
                            </p>
                        </div>
                    </div>
                </atricle>
                <a href="<?php echo base_url('noticias/post/' . $linha->slug); ?>" class="btn btn-news">Leia a notícia completa <i class="fa fa-angle-double-right"></i></a>
            </div>
        </div>
        <?php $count++;?>
        <?php else:?>
            <div class="row no-margin">
                <div class="col-lg-12 no-padding">
                    <article class="article">
                        <div class="row no-margin">
                            <div class="col-lg-8 text-right">
                                <small>publicado em: <?php echo $linha->data_publicacao ?></small>
                                <h3><?php echo $linha->titulo;?></h3>
                                <?php
                                echo $txt = character_limiter($linha->resumo, 1000, '...');
                                ?>
                            </div>
                            <div class="col-lg-4 no-padding">
                                <figure>
                                    <img src="<?php echo base_url('uploads/'.$linha->imagem); ?>" alt="<?php echo $linha->titulo;?>" />
                                </figure>
                            </div>
                        </div>
                    </atricle>
                    <a href="<?php echo base_url('noticias/post/' . $linha->slug); ?>" class="btn btn-news">Leia a notícia completa <i class="fa fa-angle-double-right"></i></a>
                </div>
            </div>
        <?php $count++;?>
        <?php endif;?>
        <?php endforeach;?>

        <nav class="text-right">
            <ul class="pagination">
                <li>
                    <a href="#" aria-label="Previous">
                        <i class="fa fa-chevron-left" aria-hidden="true"></i>
                    </a>
                </li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li>
                    <a href="#" aria-label="Next">
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</section>
