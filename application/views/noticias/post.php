<?php
    $slug = $this->uri->segment(3);
    $busca = $this->noticias_model->buscar_pela_slug($slug)->row();

    if($slug == NULL):
        redirect('noticias');
    endif;
?>


<section class="post">
    <div class="container">
        <h1><?php echo $busca->titulo;?></h1>
        <div class="row no-margin">
            <div class="col-lg-8 no-padding">
                <article class="article">
                    <div class="row no-margin">
                        <div class="col-lg-12 no-padding">
                            <figure>
                                <img src="<?php echo base_url('uploads/'.$busca->imagem); ?>" alt="" />
                            </figure>
                        </div>
                        <div class="col-lg-12 no-padding">
                            <p>
                                <?php echo $busca->texto;?>
                            </p>
                        </div>
                    </div>
                </article>
            </div>
            <div class="col-lg-4 no-padding">
                <?php $this->load->view('template/sidebar'); ?>
            </div>
        </div>
    </div>
</section>
