<header>
    <div class="header-bg" style="background-image: url(assets/imagens/header-bg.jpg);"></div>
    <div class="header-absolute"></div>
</header>


<section class="news-home">
    <div class="container">
        <h1>Notícias</h1>
        <div class="" id="owl-news">
            <?php /*for ($i=0; $i < 5; $i++):*/?>
            <?php foreach ($lista_noticias as $linha):?>
            <article class="article">
                <a href="<?php echo base_url('noticias/post/'.$linha->slug); ?>">
                    <div class="row no-margin">
                        <div class="col-lg-6 no-padding">
                            <h3><?php echo $linha->titulo;?></h3>
                                <?php echo $txt = character_limiter($linha->resumo, 1000, '...');?>
                        </div>
                        <div class="col-lg-6 no-padding">
                            <figure>
                                <img src="<?php echo base_url('uploads/'.$linha->imagem); ?>" alt="<?php echo $linha->titulo;?>" title="<?php echo $linha->titulo;?>"/>
                            </figure>
                        </div>
                    </div>
                </a>
            </article>
        <?php endforeach;?>
        <?php /*endfor; */?>
        </div>
    </div>
</section>

<section class="last-game-info">
    <div class="container">
        <div class="result">
            <h1>Último Resultado</h1>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="logo">
                            <img src="<?php echo base_url('assets/imagens/logo-bem-amil.png'); ?>" alt="" />
                            <h3>BEM AMIL FC</h3>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <h2><?php echo $lista_ultimo_resultado->placar_time;?><span>:</span><?php echo $lista_ultimo_resultado->placar_adversario;?></h2>
                        <?php if($lista_ultimo_resultado->resultado_penaltis != ''):?>
                        <h5 class="text-center thin amarelo-escuro">Resultado na disputa pro penaltis</h5>
                        <h4 class="text-center"><?php echo $lista_ultimo_resultado->resultado_penaltis;?></h4>
                        <?php endif;?>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="logo">
                            <img src="<?php echo base_url('uploads/'.$lista_ultimo_resultado->imagem); ?>" alt="" />
                            <h3><?php echo $lista_ultimo_resultado->nome_adversario;?></h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 match-name">
                    <span><?php echo $lista_ultimo_resultado->tipo_de_partida .' - '. $lista_ultimo_resultado->local;?><br><?php echo $lista_ultimo_resultado->data_partida .' - '. $lista_ultimo_resultado->horario_partida;?></span>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="team-home">
    <div class="team-parallax"></div>
    <div class="content">
        <div class="container">
            <span class="title">Em busca de mais um <strong>caneco</strong></span>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore.
            </p>
            <a href="javascript:void(0)" class="btn btn-default">Veja mais informações sobre os atletas</a>
            <div id="owl-demo">
                <?php foreach ($lista_elenco as $linha):?>
                <div class="item">
                    <figure class="player">
                        <img src="<?php echo base_url('uploads/'.$linha->imagem); ?>" alt="" />
                    </figure>
                    <p><?php echo $linha->apelido != '' ? $linha->apelido : $linha->nome ;?><br><small><?php echo $linha->posicao;?></small><big><?php echo $linha->camisa;?></big></p>
                </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</section>
