    <footer>
        <div class="overlay">
            <div class="container">
                <div class="row no-margin">
                    <div class="col-lg-7 no-padding">
                        <div class="row">
                            <div class="col-lg-12 no-padding">
                                <h1>Entre em contato</h1>
                            </div>
                        </div>
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label for="nome" class="col-sm-2 no-padding">Nome</label>
                                <div class="col-sm-10 no-padding">
                                    <input type="text" class="form-control" id="nome" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-2 no-padding">Email</label>
                                <div class="col-sm-10 no-padding">
                                    <input type="text" class="form-control" id="email" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="telefone" class="col-sm-2 no-padding">Telefone</label>
                                <div class="col-sm-10 no-padding">
                                    <input type="text" class="form-control" id="telefone" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="mensagem" class="col-sm-2 no-padding">Mensagem</label>
                                <div class="col-sm-10 no-padding">
                                    <textarea name="mensagem" class="form-control"></textarea>
                                    <input type="submit" name="name" value="Enviar" class="btn btn-success">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-4 col-lg-offset-1">
                        <h5>informações</h5>
                        <ul>
                            <li><a href="#">Notícias</a></li>
                            <li><a href="#">Nosso time</a></li>
                            <li><a href="#">O elenco</a></li>
                            <li><a href="#">Amistosos</a></li>
                            <li><a href="#">Campeonatos</a></li>
                            <li><a href="#">Artilharia</a></li>
                            <li><a href="#">Contato</a></li>
                        </ul>
                        <div class="social-media">
                            <a href="javascrip:void(0)"><i class="fa fa-2x fa-instagram" aria-hidden="true"></i></a>
                            <a href="javascrip:void(0)"><i class="fa fa-2x fa-facebook" aria-hidden="true"></i></a>
                            <a href="javascrip:void(0)"><i class="fa fa-2x fa-google-plus" aria-hidden="true"></i></a>
                            <a href="javascrip:void(0)"><i class="fa fa-2x fa-twitter" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <?php echo script_tag('assets/js/jquery-2.2.4.min.js'); ?>
    <?php echo script_tag('assets/js/bootstrap.min.js'); ?>
    <?php echo script_tag('assets/js/owl.carousel.min.js'); ?>
    <?php echo script_tag('assets/js/main.js'); ?>
</body>
</html>
