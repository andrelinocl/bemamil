<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="<?php echo base_url('assets/imagens/logo-bem-amil-24.png');?>" />

	<title><?php echo $title; ?></title>

	<?php echo link_tag('assets/font-awesome/css/font-awesome.min.css'); ?>

	<?php echo link_tag('assets/css/bootstrap.css'); ?>
	<?php echo link_tag('assets/css/owl.carousel.min.css'); ?>
	<?php echo link_tag('assets/css/main.css'); ?>
</head>
<body>
	<?php
		$uri = uri_string(1);
	?>
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<div class="bar"></div>
					<div class="bar"></div>
					<div class="bar"></div>
				</button>
				<a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/imagens/logo-bem-amil.png'); ?>" alt="" /></a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li <?php echo $uri == 'noticias' ? 'class="active"' : ''; ?>><a href="<?php echo base_url('noticias'); ?>">Notícias</a></li>
					<li <?php echo $uri == 'time' ? 'class="active"' : ''?>><a href="<?php echo base_url('time'); ?>">O Time</a></li>
					<li <?php echo $uri == 'elenco' ? 'class="active"' : '' ;?>><a href="<?php echo base_url('elenco'); ?>">Elenco</a></li>
					<li <?php echo $uri == 'jogos' ? 'class="active"' : '' ;?>><a href="<?php echo base_url('jogos'); ?>">Jogos</a></li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>
