<div class="sidebar">
    <label for="search">O que está procurando</label>
    <div class="input-group">
        <input type="text" id="search" class="form-control" aria-describedby="basic-addon2">
        <a href="javascrip:void(0)" class="input-group-addon" id="basic-addon2"><i class="fa fa-search" aria-hidden="true"></i></a>
    </div>
    <article class="article last">
        <h5>Outras notícias</h5>
        <ul>
            <?php foreach($buscar_todas as $linha):?>
            <?php if($this->uri->segment(3) != $linha->titulo): ?>
            <li><a href="<?php echo base_url('noticias/post/' . $linha->slug); ?>"><?php echo $linha->titulo;?><br><small><?php echo $linha->data_publicacao; ?></small></a></li>
            <?php endif;?>
            <?php endforeach;?>
        </ul>
    </article>
    <h5>Último Resultado</h5>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <div class="logo">
                    <img src="<?php echo base_url('assets/imagens/logo-bem-amil.png'); ?>" alt="" />
                    <h6>BEM AMIL FC</h6>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <h2><?php echo $lista_ultimo_resultado->placar_time;?><span>:</span><?php echo $lista_ultimo_resultado->placar_adversario;?></h2>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <div class="logo">
                    <img src="<?php echo base_url('uploads/'.$lista_ultimo_resultado->imagem); ?>" alt="<?php echo $lista_ultimo_resultado->nome_adversario;?>" />
                    <h6><?php echo $lista_ultimo_resultado->nome_adversario;?></h6>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 match-name">
            <span><?php echo $lista_ultimo_resultado->tipo_de_partida .' - '. $lista_ultimo_resultado->local;?><br><?php echo $lista_ultimo_resultado->data_partida . ' - '.$lista_ultimo_resultado->horario_partida;?></span>
        </div>
    </div>
    <h5>Próximo jogo</h5>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <div class="logo">
                    <img src="<?php echo base_url('assets/imagens/logo-bem-amil.png'); ?>" alt="" />
                    <h6>NMSC</h6>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <h2><span>:</span></h2>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <div class="logo">
                    <img src="<?php echo base_url('uploads/'.$lista_ultimo_resultado->imagem); ?>" alt="" />
                    <h6>LANCASTER</h6>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 match-name">
            <span>Campeonato<br>28/05/2016 - 18:00hs</span>
        </div>
    </div>
</div>
