<section class="history">
    <div class="container no-padding">
        <h1>história do clube</h1>
        <div class="row no-margin">
            <div class="col-lg-8 no-padding">
                <div class="history-body">
                    <?php for($i=0;$i<=5;$i++): ?>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                    <?php endfor; ?>
                </div>
            </div>
            <div class="col-lg-4 no-padding">
                <?php $this->load->view('includes/sidebar'); ?>
            </div>
        </div>
    </div>
</section>
