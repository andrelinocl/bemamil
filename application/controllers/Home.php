<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	private $title = 'BEM AMIL FC';

	public function __construct(){
		parent::__construct();

		$this->load->model('home_model');
	}

	public function index()
	{
		$this->data += array(
			'title' => $this->title,
			'lista_noticias' => $this->home_model->buscar_todas(),
			'lista_ultimo_resultado' => $this->home_model->busca_ultimo_resultado(),
			'lista_elenco' => $this->home_model->busca_elenco(),
		);
		$this->load->view('template', $this->data);
	}
}
