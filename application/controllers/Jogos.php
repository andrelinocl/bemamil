<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jogos extends CI_Controller {

	public function index()
	{
		$data['title'] = 'NMSC PEACHFEST';

		$this->load->view('includes/header', $data);
		$this->load->view('jogos/index');
		$this->load->view('includes/footer');
	}
}
