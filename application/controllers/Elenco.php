<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Elenco extends CI_Controller {

	public function index()
	{
		$data['title'] = 'NMSC PEACHFEST';

		$this->load->view('includes/header', $data);
		$this->load->view('elenco/index');
		$this->load->view('includes/footer');
	}
}
