<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Time extends CI_Controller {

	public function index()
	{
		$data['title'] = 'BEM AMIL FC';

		$this->load->view('includes/header', $data);
		$this->load->view('time/index');
		$this->load->view('includes/footer');
	}
}
