<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Noticias extends MY_Controller {

	private $title = 'BEM AMIL FC';

	public function __construct(){
		parent::__construct();

		$this->load->model('noticias_model');
	}

	public function index()
	{
		$this->data += array(
			'title' => $this->title,
			'buscar_todas' => $this->noticias_model->buscar_todas(),
			'lista_ultimo_resultado' => $this->noticias_model->busca_ultimo_resultado(),
			'lista_proxima_partida' => $this->noticias_model->busca_proxima_partida(),
		);
		$this->load->view('template', $this->data);
	}

	public function post($slug = NULL){
		$this->data += array(
			'title' => $this->title,
			'buscar_todas' => $this->noticias_model->buscar_todas(),
			'lista_ultimo_resultado' => $this->noticias_model->busca_ultimo_resultado(),
		);
		$this->load->view('template', $this->data);
	}
}
