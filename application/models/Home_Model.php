<?php

    class Home_Model extends CI_Model{

        private $tabela = 'noticias';

        public function buscar_todas(){
            $this->db->limit(5);
            $busca = $this->db->get($this->tabela);
            return $busca->result();
        }

        public function busca_ultimo_resultado(){
            $this->db->limit(1);
            $this->db->join('tipos_de_partida', 'tipos_de_partida.id_tipo_de_partida = partidas.tipo_de_partida_id');
            $this->db->join('adversarios', 'adversarios.id_adversario = partidas.adversario_id');
            $busca = $this->db->get('partidas');
            return $busca->row();
        }

        public function busca_elenco(){
            $this->db->join('posicao', 'posicao.id_posicao = elenco.posicao_id');
            $query = $this->db->get('elenco');
            return $query->result();
        }
    }
