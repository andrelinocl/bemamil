<?php

    class Noticias_Model extends CI_Model{

        private $tabela = 'noticias';

        public function buscar_todas(){
            $busca = $this->db->get($this->tabela);
            return $busca->result();
        }

        public function buscar_pela_slug($slug = NULL){
            $this->db->where('slug', $slug);
            $this->db->limit(1);
            return $this->db->get($this->tabela);
        }

        public function busca_ultimo_resultado(){
            $this->db->limit(1);
            $this->db->join('tipos_de_partida', 'tipos_de_partida.id_tipo_de_partida = partidas.tipo_de_partida_id');
            $this->db->join('adversarios', 'adversarios.id_adversario = partidas.adversario_id');
            $busca = $this->db->get('partidas');
            return $busca->row();
        }

        public function busca_proxima_partida(){
            $this->db->order_by('data_partida', 'DESC');
            $this->db->join('tipos_de_partida', 'tipos_de_partida.id_tipo_de_partida = partidas.tipo_de_partida_id');
            $this->db->join('adversarios', 'adversarios.id_adversario = partidas.adversario_id');
            $busca = $this->db->get('partidas');
            return $busca->result();
        }
    }
