$(document).ready(function(){


    function customPager() {
        $.each(this.owl.userItems, function (i) {
            var titleData = jQuery(this).find('img').attr('title');
            var paginationLinks = jQuery('.owl-controls .owl-pagination .owl-page span');
        $(paginationLinks[i]).append(titleData);
        });
    }


    $('.navbar-toggle').click(function(){
        $(this).toggleClass('close-menu');
    });

    $("#owl-demo").owlCarousel({
		autoPlay: false,
		items : 3,
		navigation : true,
		itemsDesktop : [1199,3],
		itemsDesktopSmall : [979,3],
		navigationText: ['<i class="fa fa-2x fa-angle-left"></i>','<i class="fa fa-2x fa-angle-right"></i>'],
    });
    $("#owl-news").owlCarousel({
		autoPlay: false,
		items : 1,
		navigation : true,
		itemsDesktop : [1199,3],
		itemsDesktopSmall : [979,3],
		navigationText : ['<i class="fa fa-2x fa-angle-left"></i>','<i class="fa fa-2x fa-angle-right"></i>'],
        dots : true,
        afterInit: customPager,
        afterUpdate: customPager
    });

    $('.team .player').hover(function(){
        $('.team .player:hover .player-info').fadeIn('slow');
    });

});
